//
//  main.m
//  POC_Quantia
//
//  Created by Lin, Jianbin on 28/09/2016.
//  Copyright © 2016 Lin, Jianbin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
