//
//  UCCoursePlayerView.h
//  Univadis
//
//  Created by Lin, Jianbin on 02/12/2016.
//  Copyright © 2016 Aptus Health. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UCUser;
@class UCCourse;

@interface UCCoursePlayerView : UIView

@property(nonatomic, copy, nullable) void (^screenModeDidChange)(BOOL isFullScreenMode);
@property(nonatomic, readonly) BOOL isFullScreenMode;

-(void)loadCourse;

@end
