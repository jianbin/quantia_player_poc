//
//  ViewController.m
//  POC_Quantia
//
//  Created by Lin, Jianbin on 28/09/2016.
//  Copyright © 2016 Lin, Jianbin. All rights reserved.
//

#import "ViewController.h"
#import "UCCoursePlayerView.h"

@import JavaScriptCore;

@interface ViewController ()<UIWebViewDelegate>

@property(nonatomic, retain) IBOutlet UCCoursePlayerView* playerView;
@property(nonatomic, assign) IBOutlet NSLayoutConstraint* playerHeight;
@property(nonatomic, assign) BOOL isStatusBarHidden;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self updatePlayerHeightWithSize:self.view.bounds.size];
    [self.playerView loadCourse];
}


-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [self updatePlayerHeightWithSize:size];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setIsStatusBarHidden:(BOOL)isStatusBarHidden{
    _isStatusBarHidden = isStatusBarHidden;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (BOOL)prefersStatusBarHidden {
    return self.isStatusBarHidden;
}

- (void)updatePlayerHeightWithSize:(CGSize)size{
    
    // Landscape
    if(size.width > size.height){
        self.playerHeight.constant = size.height - 20;
    }
    else{
        self.playerHeight.constant = size.width * 0.75;
    }
}

@end
