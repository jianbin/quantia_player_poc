//
//  AppDelegate.h
//  POC_Quantia
//
//  Created by Lin, Jianbin on 28/09/2016.
//  Copyright © 2016 Lin, Jianbin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

