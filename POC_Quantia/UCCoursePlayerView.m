//
//  UCCoursePlayerView.m
//  Univadis
//
//  Created by Lin, Jianbin on 02/12/2016.
//  Copyright © 2016 Aptus Health. All rights reserved.
//

#import "UCCoursePlayerView.h"

// Devices
#define IS_IPAD     (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

@import JavaScriptCore;

@interface UCCoursePlayerView()<UIWebViewDelegate, UIScrollViewDelegate>
@property(nonatomic, weak) UIWebView* webView;
@end


@implementation UCCoursePlayerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        UIWebView* webView = [UIWebView new];
        self.webView = webView;
        self.webView.delegate = self;
        self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        self.webView.frame = self.bounds;
        self.webView.scrollView.delegate = self;
        [self.webView.scrollView setShowsVerticalScrollIndicator:NO];
        _isFullScreenMode = NO;
        self.webView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.webView];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self adaptWebviewToContainerViewSize:self.bounds.size];
}

#pragma mark - UIWebViewDelegate
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    //DLog(@"Cannot load webview request:%@", error.localizedDescription);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self adaptWebviewToContainerViewSize:self.bounds.size];
    [self injectNativeCode];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x > 0)
        scrollView.contentOffset = CGPointMake(0, scrollView.contentOffset.y);
}

#pragma mark - Quantia player

- (void)resizePlayerInWebview:(CGSize)size orientation:(NSString*)orientation fullscreenButtonNeeded:(bool)needFullscreenButton{
    
    NSString* fullscreenButtonNeeded =  needFullscreenButton ? @"true" : @"false";
    NSString* resizeJs = [NSString stringWithFormat:@"dynamicallyResizePlayerForWebview('%@', %f, %f, false, %@);", orientation, size.width, size.height, fullscreenButtonNeeded];
    [self.webView stringByEvaluatingJavaScriptFromString:resizeJs];
}



- (void)resizeToFullscreen {
    if(self.screenModeDidChange){
        _isFullScreenMode = YES;
        self.screenModeDidChange(true);
    }
}

- (void)resizeToOriginalSize {
    if(self.screenModeDidChange){
        _isFullScreenMode = NO;
        self.screenModeDidChange(false);
    }
}

- (void)injectNativeCode{
    
    // get JSContext from UIWebView instance
    JSContext *context = [self.webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    
    context[@"sizePlayerToFullscreen"] = ^() {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self resizeToFullscreen];
        });
    };
    
    context[@"sizePlayerToOriginalSize"] = ^() {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self resizeToOriginalSize];
        });
    };
    
    NSString* jsToFullscreen = [NSString stringWithFormat:@"PlayerJavaScriptCallInterface.resizeToFullscreen = function () { sizePlayerToFullscreen(); };"];
    [self.webView stringByEvaluatingJavaScriptFromString:jsToFullscreen];
    
    NSString* jsToOriginalSize = [NSString stringWithFormat:@"PlayerJavaScriptCallInterface.resizeToOriginalSize = function () { sizePlayerToOriginalSize(); };"];
    [self.webView stringByEvaluatingJavaScriptFromString:jsToOriginalSize];
}

- (void)adaptWebviewToContainerViewSize:(CGSize)containerSize{
    NSString* orientation = @"portrait";
    // Landscape mode
    if(UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)){
        orientation = @"landscape";
        [self resizePlayerInWebview:containerSize orientation:orientation fullscreenButtonNeeded:[self shouldDisplayFullScreenButton]];
    }
    // Portrait mode
    else{
        CGFloat playerHeight = containerSize.width * 0.75;
        [self resizePlayerInWebview:CGSizeMake(containerSize.width, playerHeight) orientation:orientation fullscreenButtonNeeded:[self shouldDisplayFullScreenButton]];
    }
}

-(BOOL)shouldDisplayFullScreenButton{
    if(IS_IPAD){
        return YES;
    }
    else{
        return NO;
    }
}

-(void)loadCourse{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSString* url = [NSString stringWithFormat:@"http://uatire.quantiaco.com/service/articledirect?lang=eng-US&id=2000602&clientType=univadisApp&mmQuantiaUuid=cb3d4106-cb74-4e27-9cbf-6202925d4c22&mmQuantiaCluster=cluster_ie&mmQuantiaProfile=OOtZEMrVMUXdgQ+Sya79htLuGlJcmsobjzi2VG/Fa3ekWoUakZTI9LBg7v4E/t1HC7jDQ0Ww/Mu3XNBX1KkumIQFlWEmxnKa4DhkWZTzjUi2i5RNx9UjOg=="];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
        [self.webView loadRequest:request];
    });
}

@end
